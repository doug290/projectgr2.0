import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
  companies: any;

  constructor(private http: HttpClient) { }

  // ngOnInit happens when the component is initialized
  ngOnInit() {
    this.getCompanies();
  }

  getCompanies(){
    this.http.get('http://localhost:5000/api/companies').subscribe(response => {
      this.companies = response;
    }, error => {
      console.log(error);
    });
  }

}
