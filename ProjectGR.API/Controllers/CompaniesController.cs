using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ProjectGR.API.Data;
using Microsoft.EntityFrameworkCore;

namespace ProjectGR.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly DataContext _context;
        //injecting the database
        public CompaniesController(DataContext context)
        {
            _context = context;
        }
        // GET api/values
        //By using IActionResult you can return a http response rather than just a string
        //By using a Task it makes asyncronous
        //Returns all values
        [HttpGet]
        public async Task<IActionResult> GetValues()
        {
            var values = await _context.Companies.ToListAsync();

            return Ok(values);
        }

        // GET api/values/5
        //Returns a specific value
        [HttpGet("{id}")]
        public async Task<IActionResult> GetValue(int id)
        {
            //Returns the first elemenet OR a default value rathn tha an exeption
            var value = await _context.Companies.FirstOrDefaultAsync(x => x.Id == id);

            return Ok(value);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
    }