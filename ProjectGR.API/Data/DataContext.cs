using Microsoft.EntityFrameworkCore;
using ProjectGR.API.Models;

namespace ProjectGR.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) {}

        //Values vai ser o nome da tebela no banco que o entity vai criar
        public DbSet<Company> Companies { get; set; }
    }
}